from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/divide', methods=['POST'])
def divide():
    data = request.get_json()
    # Handle division by zero error
    num1 = data['num1']
    num2 = data['num2']
    result = 'Error' if num2 == 0 else num1 / num2
    return jsonify({'result': result})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=80)

