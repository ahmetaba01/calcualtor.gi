document.addEventListener('DOMContentLoaded', () => {
    const display = document.getElementById('display');
    let displayValue = '';

    // Function to update display
    const updateDisplay = (value) => {
        display.textContent = value;
    };

    // Function to parse the operation and numbers
    const parseOperation = (operation) => {
        const operations = {
            '+': 'add',
            '-': 'subtract',
            '*': 'multiply',
            '/': 'divide'
        };
        const operationSign = operation.match(/[\+\-\*\/]/)[0];
        const [num1, num2] = operation.split(operationSign).map(num => parseFloat(num.trim()));
        return { num1, num2, operation: operations[operationSign] };
    };

    // Function to perform the operation by sending a request to the correct service
    const performOperation = ({ num1, num2, operation }) => {
        fetch(`http://localhost:8080/${operation}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ num1, num2 }),
        })
        .then(response => response.json())
        .then(data => {
            updateDisplay(data.result.toString());
            displayValue = ''; // Clear the current operation after displaying the result
        })
        .catch(error => {
            console.error('Error:', error);
            updateDisplay('Error');
        });
    };

    // Handle button click
    document.querySelectorAll('.button').forEach(button => {
        button.addEventListener('click', (e) => {
            const buttonValue = e.target.textContent;

            // Clear the display if 'C' is pressed
            if (buttonValue === 'C') {
                displayValue = '';
                updateDisplay('0');
                return;
            }

            // Determine if the equals button was pressed
            if (buttonValue === '=') {
                if (!displayValue) return; // Do nothing if there's no operation to perform
                
                // Parse the display value for numbers and operation
                const operationData = parseOperation(displayValue);
                // Perform the operation
                performOperation(operationData);
            } else {
                // Add space around the operator for easier parsing
                if (buttonValue.match(/[\+\-\*\/]/)) {
                    displayValue += ` ${buttonValue} `;
                } else {
                    displayValue += buttonValue;
                }
                updateDisplay(displayValue);
            }
        });
    });
});
