from flask import Flask, send_from_directory, request, jsonify
import requests

app = Flask(__name__, static_folder='static')

@app.route('/')
def index():
    return send_from_directory('static', 'index.html')

@app.route('/<path:path>')
def static_files(path):
    return send_from_directory('static', path)

# Toplama işlemi için proxy endpoint
@app.route('/add', methods=['POST'])
def add_proxy():
    data = request.json
    response = requests.post('http://addition:80/add', json=data)
    return jsonify(response.json())

# Çıkarma işlemi için proxy endpoint
@app.route('/subtract', methods=['POST'])
def subtract_proxy():
    data = request.json
    response = requests.post('http://subtraction:80/subtract', json=data)
    return jsonify(response.json())

# Çarpma işlemi için proxy endpoint
@app.route('/multiply', methods=['POST'])
def multiply_proxy():
    data = request.json
    response = requests.post('http://multiplication:80/multiply', json=data)
    return jsonify(response.json())

# Bölme işlemi için proxy endpoint
@app.route('/divide', methods=['POST'])
def divide_proxy():
    data = request.json
    response = requests.post('http://division:80/divide', json=data)
    return jsonify(response.json())

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
