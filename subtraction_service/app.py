from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/subtract', methods=['POST'])
def subtract():
    data = request.get_json()
    result = data['num1'] - data['num2']
    return jsonify({'result': result})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=80)
